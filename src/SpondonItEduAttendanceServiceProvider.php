<?php

namespace SpondonIt\EduAttendanceService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\EduAttendanceService\Middleware\EduAttendanceService;

class SpondonItEduAttendanceServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(EduAttendanceService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'attendance');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'attendance');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
