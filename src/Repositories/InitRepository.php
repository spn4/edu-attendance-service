<?php

namespace SpondonIt\EduAttendanceService\Repositories;

use Illuminate\Support\Facades\Schema;
use Modules\Localization\Entities\Language;
use Modules\Setting\Model\GeneralSetting;
use Nwidart\Modules\Facades\Module;

class InitRepository
{

    public function init()
    {
        config([
            'app.item' => '32806276',
            'spondonit.module_manager_model' => false,
            'spondonit.module_manager_table' => false,

            'spondonit.settings_model' => GeneralSetting::class,
            'spondonit.module_model' => Module::class,

            'spondonit.user_model' => \App\Models\User::class,
            'spondonit.settings_table' => 'general_settings',
            'spondonit.database_file' => 'wchat.sql',
        ]);
    }


    public function config(){

        if (Schema::hasTable('languages')){
            $directories = array_map('basename', \File::directories(resource_path('lang')));
            $languages = Language::whereIn('code', $directories)->get();
        }else{
            $languages = [];
        }


        view()->composer('backend.partials._menu', function ($view) use ($languages) {
            $view->with('languages', $languages);
        });

    }

}
